const mongoose = require('mongoose');
const { Schema } = mongoose;

const transactionSchema = new Schema({
  store_name: { type: String, required: true },
  transaction_id: { type: String, required: true },
  revenue: { type: Schema.Types.Number, required: true },
  timestamp: { type: Schema.Types.Date, required: true },
  products: [ { type: Schema.Types.ObjectId, ref:'Product' } ]
}, {
  timestamps: true
});

module.exports = mongoose.model('Transaction', transactionSchema);
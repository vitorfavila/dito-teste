const mongoose = require('mongoose');
const { Schema } = mongoose;

const productSchema = new Schema({
  name: { type: String, required: true },
  price: { type: Schema.Types.Number, required: true },
}, {
  timestamps: true
});

module.exports = mongoose.model('Product', productSchema);
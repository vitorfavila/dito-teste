const mongoose = require('mongoose');
const { Schema } = mongoose;

const navigationSchema = new Schema({
  event: { type: String, required: true },
  timestamp: { type: Schema.Types.Date, required: true }
}, {
  timestamps: true
});

module.exports = mongoose.model('Navigation', navigationSchema);
const Navigation = require('../models/navigation');

module.exports = {
  insertNavigation: async (req, res) => {

    if (req.body.event && req.body.timestamp) {
      var navigation = await Navigation.create(req.body);

      res.send(navigation);
    } else {
      res.sendStatus(400);
    }

  },
  getNavigation: async (req, res) => {

    var navigations = await Navigation.find({}).populate(['sector']);

    res.send(navigations);

  },
  navigationAutocomplete: async (req, res) => {

    if (req.query.event && req.query.event.length > 1) {
      var navigations = await Navigation.find({ event: {$regex: req.query.event, $options: 'i'} }).distinct('event');

      res.send(navigations);  
    } else {
      res.sendStatus(204);
    }
    
  }
}
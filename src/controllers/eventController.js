const axios = require('axios');
const _ = require('lodash');

const Transaction = require('../models/transaction');
const Product = require('../models/product');

module.exports = {
  update: async (req, res) => {
    try {
      
      // get remote data
      var events = await axios.get('https://storage.googleapis.com/dito-questions/events.json');

      if (events.data && events.data.events) {

        var transactions = [];

        // find events 'comprou'
        var comprou = _.filter(events.data.events, ev => ev.event === 'comprou');

        // find events 'comprou-produto'
        var itens = _.filter(events.data.events, ev => ev.event !== 'comprou');
        
        const comprasPromises = comprou.map(async compra => {

          let transaction_id = _.find(compra.custom_data, cd => cd.key === 'transaction_id').value;
          var productsPromises = [];

          itens.map(item => {

            // check item transaction_id for matching
            var item_transaction_id = _.find(item.custom_data, icd => icd.key === 'transaction_id').value;
            if (transaction_id === item_transaction_id) {

              var product = Product.create({
                name: _.find(item.custom_data, icd => icd.key === 'product_name').value,
                price: _.find(item.custom_data, icd => icd.key === 'product_price').value              
              })
              productsPromises.push(product);

            }

          })

          // simultaneous database inserts
          var products = await Promise.all(productsPromises);

          var transaction = await Transaction.create({
            store_name: _.find(compra.custom_data, cd => cd.key === 'store_name').value,
            transaction_id,
            revenue: compra.revenue,
            timestamp: compra.timestamp,
            products: products
          });

          transactions.push(transaction);
        })

        // promise resolution for compras map
        await Promise.all(comprasPromises);

        res.send({ status: 'success', message: 'Data loaded successfully', timeline: _.orderBy(transactions, ['timestamp'], ['desc']) });

      } else {

        res.send({ status: 'success', message: 'No data to load' });

      }

    } catch (error) {

      res.send({ status: 'failed', message: 'Unable to load resources from remote source' });

    }
  },
  getTimeline: async (req, res) => {
    var timeline = await Transaction.find({}).populate(['products']).sort({ timestamp: 'desc' });

    res.send(timeline);
  }
}
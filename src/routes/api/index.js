var express = require('express');
var router = express.Router();
var navigationController = require('../../controllers/navigationController');
var eventController = require('../../controllers/eventController');

/* Coleta de Dados para Autocomplete */
router.post('/', navigationController.insertNavigation);

/* Entrega de dados */
router.get('/', navigationController.getNavigation);

/* Autocomplete */

router.get('/autocomplete', navigationController.navigationAutocomplete);

router.get('/updateTimeline', eventController.update);
router.get('/timeline', eventController.getTimeline);

module.exports = router;

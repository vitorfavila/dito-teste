FROM node:13-alpine

WORKDIR /node-app

COPY ./src/package*.json ./

RUN npm install 

RUN npm install cross-env -g --quiet

COPY ./src .

ENV PORT 3097
EXPOSE 3097

# delay for mongodb
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN chmod +x /wait

CMD /wait && npm run start

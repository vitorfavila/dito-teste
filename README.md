# readme

Este é um material criado por Vitor Ávila para o teste da Dito.

### Requisitos? ###

* Docker + Docker Compose

### Como usar? ###

O Node está configurado para rodar na porta 3097. Para alterar basta ajustar no arquivo Dockerfile.

1. Faça um clone deste repositório e em seguida acesse-o
2. `docker-compose up`

### Endpoints

#### Serviço de Autocomplete
Frontend 

* ```http://localhost:3097```

Autocomplete API: GET

* `http://localhost:3097/api/autocomplete?event=`

Inclusão: POST 

* `http://localhost:3097/api`

Consulta: GET 

* `http://localhost:3097/api/`

#### Manipulação de Dados
Carregar Eventos: GET 

* `http://localhost:3097/api/updateTimeline`

Consultar Eventos: GET 

* `http://localhost:3097/api/timeline`